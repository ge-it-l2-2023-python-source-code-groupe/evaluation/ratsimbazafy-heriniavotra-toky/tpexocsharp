﻿using System;
using cercle_point;
using client;
using compte;
using jeux;
using menu;

namespace TpExoCsharp;

class Program
{
    public static void Main(string[] args)
    {

        int choix = 0;

        while (choix < 1 || choix > 4)
        {
            Console.WriteLine("\n--- MENU DE TRAVAUX PRATIQUES ---\n");
            Console.WriteLine("1- TP Client & Compte Class\n2- TP Cercle & Point Class\n3- TP Jeux de dés\n4- TP Horloge Module\n5-Quitter le programme");
            Console.WriteLine("Entrer votre choix: ");

            if (int.TryParse(Console.ReadLine(), out choix))
            {
                switch (choix)
                {
                    case 1:
                        Console.Clear();
                        Compte dernierCompte = null;
                        do
                        {
                            dernierCompte = Execution_Client.Ajouter();
                            Console.WriteLine("\nVoulez-vous ajouter un autre compte ? (O/N)");
                        } while (Console.ReadLine()?.ToUpper() == "O");

                        if (dernierCompte != null)
                        {
                            Execution_Client.CompteOperation(dernierCompte);
                        }
                        else
                        {
                            Console.WriteLine("Aucun compte n'a été créé.");
                        }
                        break;

                    case 2:
                        Console.Clear();
                        Console.WriteLine("TP Cercle & Point Class");
                        Execution.Executer();
                        break;

                    case 3:
                        Console.Clear();
                        Console.WriteLine("TP Jeux de dés");
                        Jeux.lancer();
                        break;

                    case 4:
                        Console.Clear();
                        Console.WriteLine("TP Horloge Module");
                        Menu.menu_principal();
                        break;

                    case 5:
                        Console.Clear();
                        Console.WriteLine("----- Fin du Programme ");
                        return;

                    default:
                        Console.Clear();
                        Console.WriteLine("Saisie erronée. Recommencer!.");
                        break;
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Saisie non valide. Veuillez entrer votre choix.");
            }
        }
    }
}
