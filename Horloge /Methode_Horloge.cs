using System;
using System.Collections.Generic;
namespace alarm;

class Methode_Horloge
{
    public static void AfficherHorloges(List<Horloge> horloges)
    {
        Console.WriteLine("Liste des Horloges :");

        foreach (var horloge in horloges)
        {
            Console.WriteLine(horloge);
        }

        DateTime heureLocale = DateTime.Now;

        Console.WriteLine($"\n{heureLocale:HH:mm:ss} // peut être à jour en temps réel\n{TimeZoneInfo.Local.Id} \n{DateTime.UtcNow:ddd. dd MMM}\n");
        Console.Write("Appuyez sur ENTER pour continuer:");
        Console.ReadLine();
    }
    public static void AjouterHorloge(List<Horloge> horloges)
    {
        Console.WriteLine("Choisissez une ville : [Moscou, Dubaï, Mexique, Canada]");
        string ville = Console.ReadLine();

        Horloge nouvelleHorloge = new Horloge(ville);
        horloges.Add(nouvelleHorloge);

        Console.WriteLine("Votre Horloge a bien été enregistrée !");
    }

}