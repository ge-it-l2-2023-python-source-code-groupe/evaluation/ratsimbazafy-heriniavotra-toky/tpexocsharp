using System;

namespace alarm;

class Horloge
{
    public string Ville { get; }
    public string FuseauHoraire { get; }
    public DateTime HeureLocale { get; }

    public Horloge(string ville)
    {
        Ville = ville;
        FuseauHoraire = GetFuseauHoraire(ville);
        HeureLocale = DateTime.UtcNow.AddHours(int.Parse(FuseauHoraire));
    }

    public override string ToString()
    {
        return $"{Ville} - {HeureLocale:HH:mm} UTC{FuseauHoraire}";
    }

    private static string GetFuseauHoraire(string ville)
    {
        switch (ville.ToLower())
        {
            case "moscou":
                return "+3";
            case "dubaï":
                return "+4";
            case "mexique":
                return "-6";
            case "canada":
                return "-7";
            default:
                return "0";
        }
    }
}

