namespace alarm;
class Alarm
{
    public string Nom_alarme { get; set; }
    public string Heure { get; set; }
    public string Jours { get; set; }
    public bool LancerUneFois { get; set; }
    public bool EstPeriodique { get; set; }
    public bool SonnerieParDefaut { get; set; }

    public Alarm(string name, string time, string days, bool isOneTime, bool isPeriodic, bool useDefaultSound)
    {
        Nom_alarme = name;
        Heure = time;
        Jours = days;
        LancerUneFois = isOneTime;
        EstPeriodique = isPeriodic;
        SonnerieParDefaut = useDefaultSound;
    }

    public override string ToString()
    {
        string frequency = EstPeriodique ? "(périodique)" : "";
        return $"{Nom_alarme} - {Heure} {frequency} {Jours}";
    }
}