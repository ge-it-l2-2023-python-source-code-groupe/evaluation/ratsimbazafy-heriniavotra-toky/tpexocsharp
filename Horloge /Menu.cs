using System;
using alarm;
namespace menu;
class Menu
{
    public static void menu_principal()
    {

        while (true)
        {
            Console.WriteLine("--- Bienvenue dans le menu principale ---");
            Console.WriteLine("1 - Alarme");
            Console.WriteLine("2 - Horloge");
            Console.WriteLine("3 - Quitter");
            Console.Write("Quel est votre choix ? ");

            string choix = Console.ReadLine();

            switch (choix)
            {
                case "1":
                    Console.Clear();
                    Console.WriteLine("---- Alarmes ----");
                    Menu_alarm.menu_alarm();

                    break;
                case "2":
                    Console.Clear();
                    Menu_Horloge.GestionHorloges();
                    break;
                case "3":
                    Console.Clear();
                    Console.WriteLine("---- Fin du programme");
                    return;


                default:
                    Console.Clear();
                    Console.WriteLine("Choix non valide. Veuillez réessayer.");
                    break;
            }
        }
    }
}