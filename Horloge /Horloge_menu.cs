using System.Collections.Generic;
using System;
using menu;
namespace alarm;

class Menu_Horloge
{
    static List<Horloge> horloges = new List<Horloge>();

    public static void GestionHorloges()
    {
        while (true)
        {
            Console.Clear();
          
            Console.WriteLine("--- Gestion des Horloges ---");
            Console.WriteLine("1 - Voir les Horloges actives");
            Console.WriteLine("2 - Ajouter une Horloge");
            Console.WriteLine("3 - Retour au menu principal");
            Console.Write("Choix : ");

            string choix = Console.ReadLine();

            switch (choix)
            {
                case "1":
                    Console.Clear();
                    Methode_Horloge.AfficherHorloges(horloges);
                    break;
                case "2":
                    Console.Clear();
                    Methode_Horloge.AjouterHorloge(horloges);
                    break;
                case "3":
                    Console.Clear();
                    Console.WriteLine("Retour au menu principal.");
                    Menu.menu_principal();
                    return;
                default:
                    Console.WriteLine("Choix non valide. Veuillez réessayer.");
                    break;
            }
            Console.Clear();
        }
    }

}