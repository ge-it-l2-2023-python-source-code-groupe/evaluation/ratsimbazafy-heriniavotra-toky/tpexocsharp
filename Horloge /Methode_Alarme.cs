using System;
using System.Collections.Generic;
namespace alarm;



class Methode
{
    public static void VoirAlarmeActive(List<Alarm> alarms)
    {
        Console.WriteLine("Liste d'alarmes actives:");

        for (int i = 0; i < alarms.Count; i++)
        {
            Console.WriteLine(alarms[i].ToString());
        }

        Console.WriteLine("Appuyez sur une touche pour continuer...");
        Console.ReadKey();
    }

    public static void CreerAlarme(List<Alarm> alarms)
    {
        Console.WriteLine("Info de la nouvelle Alarme:");

        Console.Write("Donnez un Nom_alarme de référence: ");
        string Nom_alarme = Console.ReadLine();

        Console.Write("Heure de l'alarme (hh:mm): ");
        string Heure = Console.ReadLine();

        Console.Write("Date de planification (L/M/Me/J/V/S/D): ");
        string Jours = Console.ReadLine();

        Console.Write("Lancer une seule fois (y/n): ");
        bool LancerUneFois = Console.ReadLine().ToLower() == "y";

        Console.Write("Périodique (y/n): ");
        bool EstPeriodique = Console.ReadLine().ToLower() == "y";

        Console.Write("Activer sonnerie par défaut (y/n): ");
        bool SonnerieParDefaut = Console.ReadLine().ToLower() == "y";


        Jours = Jours switch
        {
            "L" => "Lundi",
            "M" => "Mardi",
            "Me" => "Mercredi",
            "J" => "Jeudi",
            "V" => "Vendredi",
            "S" => "Samedi",
            "D" => "Dimanche",
            _ => "Lundi, Mardi, Mercredi, Jeudi, Vendredi, Samedi, Dimanche",
        };


        Alarm nouvelAlarme = new Alarm(Nom_alarme, Heure, Jours, LancerUneFois, EstPeriodique, SonnerieParDefaut);
        alarms.Add(nouvelAlarme);
        Console.Clear();
        Console.WriteLine("Votre nouvelle alarme est enregistrée :)");

    }

}