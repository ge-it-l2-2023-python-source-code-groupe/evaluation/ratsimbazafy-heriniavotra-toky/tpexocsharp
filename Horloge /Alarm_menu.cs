using System;
using System.Collections.Generic;
using menu;


namespace alarm;

class Menu_alarm{
    static List<Alarm> alarms = new List<Alarm>();

    public static void menu_alarm()
    {
        while (true)
        {
            Console.WriteLine("--- Menu d'alarmes ---");
            Console.WriteLine("1 - Voir les Alarmes actives");
            Console.WriteLine("2 - Créer une alarme");
            Console.WriteLine("3 - Retour au menu principal");
            Console.Write("Quel est votre choix ? ");

            string choix = Console.ReadLine();

            switch (choix)
            {
                case "1":
                    Console.Clear();
                    Methode.VoirAlarmeActive(alarms);
                    break;
                case "2":
                    Console.Clear();
                    Methode.CreerAlarme(alarms);
                    break;
                case "3":
                    Console.Clear();
                    Menu.menu_principal();
                    break;
                default:
                    Console.Clear();
                    Console.WriteLine("Choix non valide. Veuillez réessayer.");
                    break;
            }
        }
    }
}