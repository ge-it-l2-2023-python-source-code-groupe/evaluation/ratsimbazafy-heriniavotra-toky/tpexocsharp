using System;
using compte;

namespace client
{
    class Client
    {
        public String Nom_client { get; set; }
        public String Prenom_client { get; set; }
        public string Cin { get; set; }
        public string Telephone { get; set; }

        public Client(string nom_client, string prenom_client, string cin, string telephone)
        {
            this.Nom_client = nom_client;
            this.Prenom_client = prenom_client;
            this.Cin = cin;
            this.Telephone = telephone;
        }
        public void Afficher()
        {
            Console.WriteLine("\n----- Infos sur compte -------\n");
            Console.WriteLine($"Nom:{Nom_client}\nPrénom:{Prenom_client}\nCIN:{Cin}\nTéléphone:{Telephone}");
        }

    }

}


