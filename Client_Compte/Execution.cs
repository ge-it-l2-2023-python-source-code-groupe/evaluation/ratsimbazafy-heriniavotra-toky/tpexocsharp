using System;
using compte;


namespace client
{
    class Execution_Client
    {
        private static Compte compte1;

        public static Compte Ajouter()
        {

            Console.Clear();
            Console.WriteLine("TP Client & Compte Class");
            Console.WriteLine("Entrer le nom:");
            String nom = Console.ReadLine();
            Console.WriteLine("Entrer le prénom:");
            String prenom = Console.ReadLine();
            Console.WriteLine("Entrer le cin:");
            String cin = Console.ReadLine();
            Console.WriteLine("Entrer le téléphone:");
            String tel = Console.ReadLine();

            Client client = new Client(nom, prenom, cin, tel);
            Compte compte = new Compte(client);
            compte.Retour();
            client.Afficher();
            compte.GetProprietaire();

            Console.WriteLine("\n****************************************");
            Console.WriteLine("Donnez le montant à déposer");
            int crediter = Int32.Parse(Console.ReadLine() ?? "");
            compte.Crediter(crediter);
            compte.GetProprietaire();

            Console.WriteLine("\n****************************************");
            Console.WriteLine("Donnez le montant à retirer");
            int debiter = Int32.Parse(Console.ReadLine() ?? "");
            compte.Debiter(debiter);
            

            if (compte1 == null)
            {
                compte1 = compte; // Stocke la référence au compte 1 lors de sa création
                compte1.GetProprietaire();
            }
            
            return compte;
            

        }
        public static void CompteOperation(Compte compte2)
        {
            if (compte2 != null)
            {
                Console.WriteLine("\n****************************************");
                Console.WriteLine($"Donnez le montant à déposer sur le compte {compte2.Id_compte} à partir du compte {compte1.Id_compte}:");
                int montantCrediterCompte2 = Int32.Parse(Console.ReadLine() ?? "");
                compte1.Transferer(compte2, montantCrediterCompte2);
                Console.WriteLine("Opération bien effectuée");
                compte1.GetProprietaire();
                compte2.GetProprietaire();

                // Opération de débit et crédit
                Console.WriteLine("\n****************************************");
                Console.WriteLine("Débiter le compte 1 et créditer le compte 2");
                Console.WriteLine("Donnez le montant à retirer: ");
                int montantDebiterCompte1CrediterCompte2 = Int32.Parse(Console.ReadLine() ?? "");
                compte1.Debiter(montantDebiterCompte1CrediterCompte2);
                compte2.Crediter(montantDebiterCompte1CrediterCompte2);
                Console.WriteLine("Opération bien effectuée");
                compte1.GetProprietaire();
                compte2.GetProprietaire();
            }
            else
            {
                Console.WriteLine("Erreur : Compte est null.");
            }
        }
    }
}