using client;
using System;
namespace compte;

class Compte
{
    public int Id_compte { get; }
    public int Solde { get; set; }

    private Client Proprietaire;

    private static int compteur = 0;


    static Compte()
    {
        compteur = 0;
    }

    public Compte(Client proprietaire)
    {
        Id_compte = ++compteur;
        Proprietaire = proprietaire;
        Solde = 0;
    }

    public void Retour()
    {
        Console.WriteLine("Compte " + Id_compte);

    }

    public void GetProprietaire()
    {
        Console.WriteLine("****************************************");
        Console.WriteLine($"\nLe proprietaire du compte: \nNumero du compte: {Id_compte}\nNom:{Proprietaire.Nom_client} \nPrenom:{Proprietaire.Prenom_client} \nCIN:{Proprietaire.Cin} \nTelephone:{Proprietaire.Telephone}\nSolde:{Solde}");
    }
    public void Crediter(int crediter)
    {

        Solde = Solde + crediter;
        Console.WriteLine($"Opération bien effectué");

    }
    public void Debiter(int debiter)
    {

        if (debiter <= Solde)
        {
            Solde -= debiter;
            Console.WriteLine($"Opération bien effectué");
        }
        else
        {
            Console.WriteLine("Solde insuffisant pour effectuer l'opération.");
        }


    }
    public void Transferer(Compte compteDestination, int montant)
    {
        if (montant <= Solde)
        {
            Debiter(montant);
            compteDestination.Crediter(montant);
            Console.WriteLine($"Opération de transfert bien effectuée. Nouveau solde du compte {Id_compte}: {Solde}");
        }
        else
        {
            Console.WriteLine("Solde insuffisant pour effectuer l'opération de transfert.");
        }
    }

}