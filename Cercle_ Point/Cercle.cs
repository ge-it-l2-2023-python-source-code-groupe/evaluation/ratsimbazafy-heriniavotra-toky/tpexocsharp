using System;

namespace cercle_point;

partial class Cercle{
    //attribut de type classe 
    public Point  Monpoint {get; set;} 
    public int Rayon {get; set;}
   

    //constructeur 
    public Cercle(Point monpoint, int rayon){
        Monpoint = monpoint;
        Rayon = rayon;
    }

    //Methode getPerimeter()
    public void GetPerimeter(){

        Console.WriteLine($"Le périmètre est:{Math.Round(2 * Math.PI * Rayon, 2)}" );  

    }

    //Methode getSurface()
    public void GetSurface()
    {

        Console.WriteLine($"La surface est:{Math.Round(Math.PI * Rayon * Rayon, 2)}");

    }

    //Methode Display()
    public void Display(){
        Console.WriteLine($"CERCLE({Monpoint.X},{Monpoint.Y},{Rayon})");
    }

    public bool IsInclude(Point p)
    {
        double pts = Math.Pow(p.X - Monpoint.X, 2) + Math.Pow(p.Y - Monpoint.Y, 2);
        double R = Math.Pow(Rayon, 2);

        return pts <= R;
    }
}