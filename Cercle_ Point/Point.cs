using System;

namespace cercle_point;

partial class Point
{
    //attribut x et y
    public int X { get; set; }
    public int Y { get; set; }

    //Constructeur 
    public Point(int x, int y)
    {
        X = x;
        Y = y;
    }

    //Methode Display
    public void Display()
    {
        Console.WriteLine($"POINT({X},{Y})");
    }
}