using System;

namespace cercle_point;

partial class Execution{
    public static void Executer(){
         Console.WriteLine("Donner l'abscisse du centre:");
        int x= Convert.ToInt16(Console.ReadLine());
        Console.WriteLine("Donner l'ordonnée du centre:");
        int y = Convert.ToInt16(Console.ReadLine());
        Console.WriteLine("Donner le rayon:");
        int r = Convert.ToInt16(Console.ReadLine());

        //creation d'instance Point
        Point pt1= new Point(x,y);
     
        //meme parametre que son constructeur
        Cercle cercle= new Cercle(pt1,r);
        cercle.Display();
        cercle.GetPerimeter();
        cercle.GetSurface();

        Console.WriteLine("Donnez un point:");
        Console.Write("X:");
        int pointX = Convert.ToInt32(Console.ReadLine());
        Console.Write("Y:");
        int pointY = Convert.ToInt32(Console.ReadLine());

        Point point = new Point(pointX, pointY);
        

        if (cercle.IsInclude(point))
            Console.WriteLine("Le point appartient au cercle.");
        else
            Console.WriteLine("Le point n'appartient pas au cercle.");
    }
}