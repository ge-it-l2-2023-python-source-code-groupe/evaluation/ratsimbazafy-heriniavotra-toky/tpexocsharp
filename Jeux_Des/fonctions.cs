using System;
using System.Linq;
using jeux;

namespace fonctions
{
    public class Instructions
    {
        // Partie 1 : Champs, propriétés, constructeur, méthodes non implémentées

        private static string[] nomIndividuels;
        private static int[] scores;
        private static Manche[] historiqueManches = new Manche[100];
        private static int nombreManchesJouees = 0;

        public static void NouvelleManche(int nombreDeJoueurs)
        {
            string joueursAvecTirage6;
            string nomGagnant;
            int nombreDeJoueursTire6;

            JouerTour(out joueursAvecTirage6, out nomGagnant, out nombreDeJoueursTire6);
            AfficherScoresApresTour(joueursAvecTirage6, nomGagnant, nombreDeJoueursTire6);
            Manche nouvelleManche = new Manche
            {
                DateHeure = DateTime.Now,
                NomJoueur = nomGagnant,
                Score = scores[0],
                PointsGagnes = nombreDeJoueursTire6
            };

            historiqueManches[nombreManchesJouees] = nouvelleManche;
            nombreManchesJouees++;
        }

        public static void SaisirNomsJoueurs(int nombreDeJoueurs, out string nomsJoueurs)
        {
            nomsJoueurs = "";
            nomIndividuels = new string[nombreDeJoueurs];
            scores = new int[nombreDeJoueurs];

            for (int i = 0; i < nombreDeJoueurs; i++)
            {
                Console.Write("Entrez le nom du joueur {0}: ", i + 1);
                string nomJoueur = Console.ReadLine() ?? "";

                if (nomsJoueurs == "")
                {
                    nomsJoueurs += nomJoueur;
                }
                else
                {
                    nomsJoueurs += "," + nomJoueur;
                }

                Joueur joueur = new Joueur(nomJoueur);
                joueur.afficher();
                nomIndividuels[i] = nomJoueur;
                scores[i] = 0;
            }
        }

        private static void JouerTour(out string joueursAvecTirage6, out string nomGagnant, out int nombreDeJoueursTire6)
        {
            joueursAvecTirage6 = "";
            nomGagnant = "";
            nombreDeJoueursTire6 = 0;

            for (int i = 0; i < nomIndividuels.Length; i++)
            {
                string nom = nomIndividuels[i];
                int tourJoueurPoints = 0;

                Console.WriteLine($"\nTour de {nom} - Voulez-vous lancer le dé (y ou n):");
                string choix = Console.ReadLine() ?? "";

                if (choix == "y")
                {
                    Joueur lancerDes = new Joueur(nom);
                    lancerDes.jouer();

                    int Tirage = lancerDes.points;

                    if (Tirage == 6)
                    {
                        joueursAvecTirage6 += $"{nom},";
                        nombreDeJoueursTire6++;
                        nomGagnant = nom;
                        tourJoueurPoints += 0;
                    }
                    else
                    {
                        Console.WriteLine($"{nom} n'a pas tiré un 6");
                    }
                    Console.WriteLine($"nombre de personne ayant tiré 6: {nombreDeJoueursTire6}");
                }
                else
                {
                    Console.WriteLine($"{nom} a choisi de ne pas lancer le dé");
                }

                scores[i] += tourJoueurPoints;
            }
        }

        private static void AfficherScoresApresTour(string joueursAvecTirage6, string nomGagnant, int nombreDeJoueursTire6)
        {
            Console.WriteLine("\n--- Scores ---");

            for (int i = 0; i < nomIndividuels.Length; i++)
            {
                string nom = nomIndividuels[i];
                int tourJoueurPoints = 0;

                if (joueursAvecTirage6.Split(',').Contains(nom) && nom == nomGagnant && nombreDeJoueursTire6 == 1)
                {
                    tourJoueurPoints += 2;
                }
                else if (joueursAvecTirage6.Split(',').Contains(nom) && nombreDeJoueursTire6 > 1)
                {
                    tourJoueurPoints += 1;
                }

                scores[i] += tourJoueurPoints;

                if (tourJoueurPoints < 2)
                {
                    Console.WriteLine($"{nom}: {scores[i]} point");
                }
                else
                {
                    Console.WriteLine($"{nom}: {scores[i]} points");
                }
            }
        }

        public static void VoirHistoriqueManches()
        {
            Console.WriteLine("--- Historique des Manches ---");

            for (int i = 0; i < nombreManchesJouees; i++)
            {
                Console.WriteLine($"Date et heure : {historiqueManches[i].DateHeure}");
                Console.WriteLine($"Joueur : {historiqueManches[i].NomJoueur}");
                Console.WriteLine($"Score : {historiqueManches[i].Score}");
                Console.WriteLine($"Points gagnés : {historiqueManches[i].PointsGagnes}");
                Console.WriteLine();
            }

            Console.WriteLine("Appuyez sur une touche pour retourner au menu principal.");
            Console.ReadKey();
        }


        public static void AfficherMenu()
        {
            Console.WriteLine("Menu principal");
            Console.WriteLine("1. Nouvelle Manche");
            Console.WriteLine("2. Voir l'historique des manches");
            Console.WriteLine("3. Quitter");
            Console.Write("Faites votre choix : ");
        }

       
    }

}
