using System;
namespace jeux;

class Jeux
{
    public static void lancer()
    {
        Console.WriteLine("Combien de joueurs souhaitez-vous ajouter ?");
        int nombreDeJoueurs;

        while (!int.TryParse(Console.ReadLine(), out nombreDeJoueurs) || nombreDeJoueurs <= 0)
        {
            Console.WriteLine("Veuillez entrer un nombre valide de joueurs (entier positif supérieur à zéro).");
        }

        fonctions.Instructions.SaisirNomsJoueurs(nombreDeJoueurs, out string nomsJoueurs);

        int numero = 0;

        while (numero != 3)
        {
            Console.WriteLine("--------Choix-------");
            fonctions.Instructions.AfficherMenu();

            while (!int.TryParse(Console.ReadLine(), out numero) || numero < 1 || numero > 3)
            {
                Console.Clear();
                Console.WriteLine("Veuillez entrer un choix valide (1, 2, ou 3).");
                fonctions.Instructions.AfficherMenu();
            }

            switch (numero)
            {
                case 1:
                    Console.Clear();
                    fonctions.Instructions.NouvelleManche(nombreDeJoueurs);
                    break;

                case 2:
                    Console.Clear();
                    fonctions.Instructions.VoirHistoriqueManches();
                    break;

                case 3:
                    Console.WriteLine("Au revoir");
                    break;

                default:

                    Console.WriteLine("Le numéro n'est ni 1, ni 2, ni 3");

                    break;
            }
        }
    }

}