using System;

namespace jeux ;

public class Joueur
{
    public string nom;
    public int points;

    public Joueur(string nom)
    {
        this.nom = nom;
        this.points = 0;
    }

    public void jouer()
    {
        // Simuler le points de dés
        Random random = new Random();
        this.points = random.Next(1, 7);
        if (points == 1)
        {
            Console.WriteLine("|     |\n|  *  |\n|     |");
        }
        if (points == 2)
        {
            Console.WriteLine("|*    |\n|     |\n|    *|");
        }
        if (points == 3)
        {
            Console.WriteLine("|*    |\n|  *  |\n|    *|");
        }
        if (points == 4)
        {
            Console.WriteLine("|*    *|\n|      |\n|*    *|");
        }
        if (points == 5)
        {
            Console.WriteLine("|*   *|\n|  *  |\n|*   *|");
        }
        if (points == 6)
        {
            
            Console.WriteLine("|*   *|\n|*   *|\n|*   *|");
            
        }
    }

    public void afficher()
    {
        Console.WriteLine($"Joueur {nom} créé.");
    }
}
public class Manche
    {
        public DateTime DateHeure { get; set; }
        public string NomJoueur { get; set; }
        public int Score { get; set; }
        public int PointsGagnes { get; set; }
    }

   
    